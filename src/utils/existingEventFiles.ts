import { promises as fs } from 'fs'
import * as path from 'path'

export default async function getExistingEventFiles (buildPath: string): Promise<{[hash: string]: string}> {
  try {
    const files = await fs.readdir(path.join(buildPath, 'events'))
    const obj = {}
    for (const file of files) {
      const f = file.match(/.*-([a-zA-Z0-9]+)\.html/)
      if (!f) continue
      obj[f[1]] = file
    }
    return obj
  } catch (err) {
    console.error(err)
    return {}
  }
}
